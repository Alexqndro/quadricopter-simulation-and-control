# Quadricopter simulation and control

## Description
Simulation and control of a quadricopter, the aim of the project is to build a functioning flight controller for a quadricopter that can deal with: 
- Sensor acquisition
- Navigation
- Guidance
- Control
First the algorithms will be developed and tested on matlab, then they will be ported in an embedded microcontroller such as Arduino uno or ESP32 through matlab auto-coder.

## Usage
TODO

## Sensors calibration
Calibration routine for the IMU

## Support & Contributing
For any questions and suggestions, or if you want to contribute, write to alessandrodelduca.96@gmail.com

## Authors and acknowledgment
Author: Alessandro Del Duca

## License
All the code in the repository is under GNU General public License 3.0v 

## Project status
Running