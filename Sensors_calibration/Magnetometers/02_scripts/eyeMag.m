function [A, b, B] = eyeMag(data)
% Function eyeMag
% Summary: The eyeMag function compute the Hard Iron correction from the
% data, assuming no Soft Iron effects.
%
% Input: Data matrix [Nx3]
%
% Output:
% A: Soft Iron correction, eye matrix (3x3) - No soft iron correction
% b: Hard Iron correction, vector (3x1)
% B: Magnetic field 
%
% Description:
% - The ellipsoid has equation: (X - b)'(X - b) = B^2, X'X - 2*Xb + b'b - B^2 = 0
% - The residual are  R = Y - X*solx 
%                     ri = xi^2 + yi^2 + zi^2 - 2xi*wx - 2yi*wy - 2zi*wz + wx^2 + wy^2 + wz^2 - B^2
%                     solx = [ 2bx; 2by; 2bz; B^2 - bx^2 - by^2 - bz^2]

x = data(:,1);
y = data(:,2);
z = data(:,3);

Y = x.*x + y.*y + z.*z;

X = [x y z ones(numel(x),1)]; 

solx = X\Y; % solve it in a least square sense

A = eye(3);

b = 0.5*solx(1:3);

B = sqrt(solx(4) + sum(b.*b));
end
