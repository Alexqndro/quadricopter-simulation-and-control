clearvars 
close all
clc

addpath('../01_data/')

data = readtable('log1.csv'); 
%data = load('LOG_6.dat');
d = data{:,1:3};

for i = 1:length(d)
    d(i,3) = d(i,3);
end

scatter3(d(:,1), d(:,2), d(:,3));

axis equal;
xlabel("x-axis");
ylabel("y-axis");
zlabel("z-axis");
title("Raw data")

%% Eye method
[A_eye, b_eye,B_eye,d_eye_corrected] = correctMag(d,'eye');
[eye_res, eye_err] = residualMag (A_eye, b_eye, B_eye, d);

figure
scatter3(d(:,1), d(:,2), d(:,3));
hold all
scatter3(d_eye_corrected(:,1),...
         d_eye_corrected(:,2),...
         d_eye_corrected(:,3));
     
axis equal;
xlabel("x-axis");
ylabel("y-axis");
zlabel("z-axis");
title("Eye corrected data")

%% Diagonal method
[A_diag, b_diag,B_diag,d_diag_corrected] = correctMag(d,'diag');
[diag_res, diag_err] = residualMag (A_diag, b_diag, B_diag, d);

figure
scatter3(d(:,1), d(:,2), d(:,3));
hold all
scatter3(d_diag_corrected(:,1),...
         d_diag_corrected(:,2),...
         d_diag_corrected(:,3));
     
axis equal;
xlabel("x-axis");
ylabel("y-axis");
zlabel("z-axis");
title("Diagonal corrected data")

%% Symmetric method
[A_sym, b_sym, B_sym,d_sym_corrected] = correctMag(d,'sym');
[sym_res, sym_err] = residualMag (A_sym, b_sym, B_sym, d);

figure
scatter3(d(:,1), d(:,2), d(:,3));
hold all
scatter3(d_sym_corrected(:,1),...
         d_sym_corrected(:,2),...
         d_sym_corrected(:,3));
     
axis equal;
xlabel("x-axis");
ylabel("y-axis");
zlabel("z-axis");
title("Symmetric corrected data")





%% Pameters, errors and plots comparison

disp('Soft iron correction') 
disp(A_eye) 
disp(A_diag) 
disp(A_sym) 
disp('Hard iron correction') 
disp(b_eye) 
disp(b_diag) 
disp(b_sym) 

if sym_err < diag_err && sym_err < eye_err
    disp('The best methods for this data is sym')
    disp(sym_err)
elseif diag_err < eye_err
    disp('The best methods for this data is diag')
    disp(diag_err)
else
    disp('The best methods for this data is eye')
    disp(eye_err)
end
%{
data_nord = readtable('magnord.csv');
dn = data_nord{:,1:3};
dn_corrected = (dn - b_diag')*A_diag;
%}