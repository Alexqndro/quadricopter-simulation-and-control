function [A,b,B,corrected_data] = correctMag(data,string) 
% Function correctMag 
% Summary: The correctMag fuction compute the corrected data from the data, with a
% method specified in the string input. 
%
% Methods:
% string = 'eye' -> eyeMag
% string = 'diag' -> diagMag
% string = 'sym' -> symMag
%
% Input
% Data matrix (Nx3)
% string: method, string{'eye','diag','sym'}
%
% Output
% A: Soft Iron correction, matrix (3,3) 
% b: Hard Iron correction, vector (3,1)
% B: Magnetic field 
% corrected_data: data, matrix (N,3)

switch string
    case 'eye'
        [A,b,B] = eyeMag(data);
    case 'diag'
        [A,b,B] = diagMag(data);
    case 'sym'
        [A,b,B] = symMag(data);
    otherwise
        disp("Invalid calibration method")
end
corrected_data = (data - b')*A;