function [Residual,inError] = residualMag (A,b,B,data)
% Function residualMag
% Summary: The residualMag function return the residual between the
% corrected data and the sphere centered in 0 of radius B^2, and the total inSample error
%
% Input: 
% A: Soft Iron correction, matrix (3x3)
% b: Hard Iron correction, vector (3x1)
% B: Expected magnetic field, scalar
% Data matrix (Nx3)
%
% Output: 
% Residual, scalar
% inError, scalar

N = max(size(data));

sphere = (A * (data.' - b)).'; % a point on the unit sphere

modulus_radius = sum(sphere.^2,2);

Residual = modulus_radius - B.^2; % Residual in a square sense (L2-norm)  

inError = (1/(2*B*B))*sqrt(Residual.'*Residual/N); % InSample total error

end