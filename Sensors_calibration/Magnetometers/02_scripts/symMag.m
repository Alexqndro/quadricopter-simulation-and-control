function [A,b,B] = symMag(data)
% Function symMag
% Summary: The symMag function compute the Hard and Soft Iron correction from the
% data, assuming the Soft Iron effects in generic directions.
%
% Input: Data matrix (Nx3)
%
% Output:
% A: Soft Iron correction, symmetric matrix (3x3) 
% b: Hard Iron correction, vector (3x1)
% B: Magnetic field magnitude, scalar
% 
% Description:
% The concept is analogous to the diagMag function, the mathematical formulation is cumbersome and 
% therefore it's skipped. For mor information, read the calibration report.

x = data(:,1);
y = data(:,2);
z = data(:,3);

 Y = [...
        x.*x, ...
        2*x.*y, ...
        2*x.*z, ...
        y.*y, ...
        2*y.*z, ...
        z.*z, ...
        x, ...
        y, ...
        z, ...
        ones(size(x))];
    
D = Y'*Y;

[eigvect, eigval] = eig(D);

eigvals = diag(eigval);

[~, index] = min(eigvals); %least square solved with Lagrange multiplier

solx = eigvect(:,index); 

R = solx([1 2 3; ...
          2 4 5; ...
          3 5 6]); %R must be symmetric
dR = det(R);

if dR < 0 
        R = -R; 
        solx = -solx;
        dR = -dR; 
end
    b = -0.5*(R\solx(7:9)); %hard iron offset

       B = sqrt(abs(sum([...
        R(1,1)*b(1)*b(1), ...
        2*R(2,1)*b(2)*b(1), ...
        2*R(3,1)*b(3)*b(1), ...
        R(2,2)*b(2)*b(2), ...
        2*R(3,2)*b(2)*b(3), ...
        R(3,3)*b(3)*b(3), ...
        -solx(end)] ...
    )));


 Rnew = R./nthroot(dR,3);% Normalization of R -> det(Rnew) = 1 
 
 A = sqrtm(Rnew); %soft iron offset 
 
 B = B./sqrt(nthroot(dR,3));% Normalization of B
end

