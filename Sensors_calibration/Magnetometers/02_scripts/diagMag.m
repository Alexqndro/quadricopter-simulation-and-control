function [A,b,B] = diagMag(data)
% Function diagMag
% Summary: The diagMag function compute the Hard and Soft Iron correction from the
% data, assuming the Soft Iron effects parallel to the x, y, z directions 
%
% Input: Data matrix (Nx3)
%
% Output:
% A: Soft Iron correction, diagonal matrix (3x3) 
% b: Hard Iron correction, vector (3x1)
% B: Magnetic field 
%
% Description:
% - The ellipsoid has equation: (X-b)'S(X-b) = B^2, X'S X -2*b'S X +b'S b -B^2 = 0
% - The residual are  R = D*solx
%                     ri = Sx^2*xi^2 + Sy^2*yi^2 + Sz^2*zi^2 - 2bx*Sx^2*xi - 2by*Sy^2*yi - 2bz*Sz^2*zi + Sx^2*bx^2 + Sy^2*by^2 + Sz^2*bz^2 - B^2 
%                     solx = [ Sx^2; Sy^2; Sz^2; -2bx*Sx^2;  -2by*Sy^2;  -2bz*Sz^2; bx*Sx^2 + by*Sy^2 + bz*Sz^2 - B^2]

x = data(:,1);
y = data(:,2);
z = data(:,3);

Y = [...
        x.*x, ...
        y.*y, ...
        z.*z, ...
        x, ...
        y, ...
        z, ...
        ones(size(x))];
    
D = Y'*Y;

[eigvect, eigval] = eig(D);

eigvals = diag(eigval);

[~, index] = min(eigvals); %least square solved with Lagrange multiplier

solx = eigvect(:,index); 

R = diag(solx(1:3));

dR = det(R);

if dR < 0
        R = -R; 
        solx = -solx;
        dR = -dR;
end
    b = -0.5*(solx(4:6)./solx(1:3)); %hard iron offset

    B = sqrt(abs(sum([...
        R(1,1)*b(1)*b(1), ...
        R(2,2)*b(2)*b(2), ...
        R(3,3)*b(3)*b(3), ...
        -solx(end)] ...
    )));

 Rnew = R./nthroot(dR,3);% Normalization of R -> det(Rnew) = 1 
 
 A = sqrtm(Rnew); %soft iron offset 
 
 B = B./sqrt(nthroot(dR,3));% Normalization of B
end
