function theta_t = TempInterp (T,T_ex,Theta)
% Function TempInterp
% Summary: The TempInterp function compute the interpolation of the
% parameter vectors based on differents temperatures
%
% Input:
% T: Temperature, scalar
% Tex: Temperature vector, (Ntemp,1)
% Theta: matrix of the parameters, each row correspond to a parameter
% vector theta(T(i)) = [Ai(1,1) Ai(1,2)...bi(1),bi(2),bi(3)], matrix (Ntemp,9)
%
% Output:
% theta_t: Interpolated parameter vector, vector (1,9)
%
% Description:
%
%

T_ex = sort(T_ex);

if T < T_ex(1) || T > T_ex(end)
   error("T out of range");
end

for k = 1:length(T_ex)
    if T == T_ex 
        error("T coincide with T_ex" +k+" ");
    end
    if T > T_ex(k) && T < T_ex(k+1)
    indx = k;
    end
end

L0 = ((T - T_ex(indx-1))*(T - T_ex(indx))) / ((T_ex(indx+1) - T_ex(indx-1))*(T_ex(indx+1) - T_ex(indx)));
L1 = ((T - T_ex(indx-1))*(T - T_ex(indx+1))) / ((T_ex(indx) - T_ex(indx-1))*(T_ex(indx) - T_ex(indx+1)));
L2 = ((T - T_ex(indx))*(T - T_ex(indx+1))) / ((T_ex(indx-1) - T_ex(indx+1))*(T_ex(indx-1) - T_ex(indx)));

theta_t = L0*Theta(indx+1,:) + L1*Theta(indx,:)+L2*Theta(indx-1,:);
end