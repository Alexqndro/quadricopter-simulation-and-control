clearvars
close all 
clc


%% Generate N samples at a sampling rate of Fs with a sinusoidal frequency
%% of Fc.

params = gyroparams;

N = 1000;
Fs = 100;
Fc = 0.25;

t = (0:(1/Fs):((N-1)/Fs)).';
acc = zeros(N, 3);
angvel = zeros(N, 3);
angvel(:,1) = sin(2*pi*Fc*t);
angvel(:,2) = sin(2*pi*Fc*t);
angvel(:,3) = sin(2*pi*Fc*t);

%% Non ideality in the Gyroscope 
imu = imuSensor('SampleRate', Fs, 'Gyroscope', params);
imu.Gyroscope.AxesMisalignment = [9 5 0]; % percent
imu.Gyroscope.ConstantBias = [0.4 0.3 0.5]; % rad/s
imu.Gyroscope.NoiseDensity = 1.25e-2; % (rad/s)/sqrt(Hz)

[~, gyroData] = imu(acc, angvel);

%% Plots before calibration
figure
plot(t, angvel(:,1), '--', t, gyroData(:,1))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Biased Gyroscope Data')
legend('x (ground truth)','x (gyroscope)')


figure
plot(t, angvel(:,2), '--', t, gyroData(:,2))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Biased Gyroscope Data')
legend('y (ground truth)','y (gyroscope)')


figure
plot(t, angvel(:,3), '--', t, gyroData(:,3))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Biased Gyroscope Data')
legend('z (ground truth)','z (gyroscope)')

figure   %plot of misalignment between x and y
plot(t, angvel(:,1:2), '--', t, gyroData(:,1:2))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Misaligned Gyroscope Data')
legend('x (ground truth)', 'y (ground truth)', ...
    'x (gyroscope)', 'y (gyroscope)')

%% Calibration Algorithm using least square 
Omega = [gyroData  ones(N,1)]; %Matrix with the real data of the gyro and a ones column for the Bias 

eta_x = (Omega'*Omega\Omega')*angvel(:,1); %Vector for x axes with the parameters of this model, that are tuned in a way that minimize the
                                           %least square error between the measured data and the expected data 
r_x = angvel(:,1) - Omega*eta_x; % X residual error

gyroCorr_x = Omega*eta_x;



eta_y = (Omega'*Omega\Omega')*angvel(:,2); % Same of eta_x

r_y = angvel(:,2) - Omega*eta_y; %Residual error for y

gyroCorr_y = Omega*eta_y;



eta_z = (Omega'*Omega\Omega')*angvel(:,3); % same of eta_x

r_z = angvel(:,3) - Omega*eta_z; % Residual error for z

gyroCorr_z = Omega*eta_z;


r = [r_x r_y r_z]; % Vector of residual error

E_in = r * r'; % Least square error that is the sum of all the residual

%% Plots of calibrated gyroscope
figure
plot(t, angvel(:,1), '--', t, gyroCorr_x)
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Corrected Gyroscope Data')
legend('x (ground truth)','x (gyroscope_corrected)')


figure
plot(t, angvel(:,2), '--', t, gyroCorr_y)
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Corrected Gyroscope Data')
legend('y (ground truth)','y (gyroscope_corrected)')


plot(t, angvel(:,3), '--', t, gyroCorr_z)
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Corrected Gyroscope Data')
legend('z (ground truth)','z (gyroscope_corrected)')


figure   %plot of misalignment between x and y after calibration
plot(t, angvel(:,1:2), '--', t, gyroCorr_x, t, gyroCorr_y)
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Misaligned Gyroscope Data')
legend('x (ground truth)', 'y (ground truth)', ...
    'x (gyroscope_corr)', 'y (gyroscope_corr)')