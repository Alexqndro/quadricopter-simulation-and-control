function [A,b,E,gyro_corrected] = gyroCal(data,gyro_ref)
% Function gyroCal
% Summary: The gyroCal function compute gyroscopes bias and scale/misalignment matrix 
% from the data
%
% Input: 
% Data matrix (N,3)
% gyro_ref, matrix (N,3)
%
% Output:
% A: scale/misalignment matrix, matrix (3,3)
% b: gyro bias, vector (3,1)
% E: Residual error vector (3,1)
% Gyro corrected, matrix (N,3)
%
% Description:
% For each axis r = i_ref - wix*x + wiy*y + wiz*z + bi
% In matrix form R = Ome*soli with Ome = [x, y, z, ones(length(i_ref)]

x_ref = gyro_ref(:,1);
y_ref = gyro_ref(:,2);
z_ref = gyro_ref(:,3);

Ome = [data ones(length(x_ref))];

solx = Ome\x_ref;

soly = Ome\y_ref;

solz = Ome\z_ref;
 

A = [ solx(1) solx(2) solx(3);
      soly(1) soly(2) soly(3);
      solz(1) solz(2) solz(3);];

b = [solx(4);soly(4);solz(4)];

rx = x_ref - Ome*solx;
ry = y_ref - Ome*soly;
rz = z_ref - Ome*solz;

E = [rx'*rx ry'*ry rz'*rz];

gyro_corrected = A*data' + b;
gyro_corrected = gyro_corrected';
end
