clearvars
close all
clc
%% Gyro params
params = gyroparams;
Fs = 100;
imu = imuSensor('SampleRate', Fs, 'Gyroscope', params);
imu.Gyroscope.Resolution = 0.01; % (rad/s)/LSB
imu.Gyroscope.AxesMisalignment = [2 5 1]; % percent
imu.Gyroscope.ConstantBias = [0.4 0.2 0.3]; % rad/s
imu.Gyroscope.NoiseDensity = 1.25e-2; % (rad/s)/sqrt(Hz)
%% Generated ground truth
% Generate N samples at a sampling rate of Fs with an angular speed of w = 100 RPM.
N = 100;
w = 100;    % [RPM]
w = w*2*pi/60; % w [rad/s]

t = (0:(1/Fs):((3*N-1)/Fs)).';
acc = zeros(3*N, 3);
angvel = zeros(3*N, 3);
angvel(1:N,1) = w*ones(N,1);
angvel(N+1:2*N,2) = w*ones(N,1);
angvel(2*N+1:3*N,3) = w*ones(N,1);
%% Simulated Imu
[~, gyroData] = imu(acc, angvel);

figure
plot(t, angvel(:,1), '--', t, gyroData(:,1))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Gyroscope Data')
legend('x (ground truth)', 'x (gyroscope)')

figure
plot(t, angvel(:,2), '--', t, gyroData(:,2))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Gyroscope Data')
legend('y (ground truth)', 'y (gyroscope)')

figure
plot(t, angvel(:,3), '--', t, gyroData(:,3))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('Gyroscope Data')
legend('z (ground truth)', 'z (gyroscope)')


%% Calibration
[A,b,E,gyro] = gyroCal(gyroData,angvel);

figure
plot(t, angvel(:,1), 'o', t, gyro(:,1))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('corrected Gyroscope Data')
legend('x (ground truth)', 'x (corrected)')

figure
plot(t, angvel(:,2), 'o', t, gyro(:,2))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('corrected Gyroscope Data')
legend('y (ground truth)', 'y (corrected)')

figure
plot(t, angvel(:,3), 'o', t, gyro(:,3))
xlabel('Time (s)')
ylabel('Angular Velocity (rad/s)')
title('corrected Gyroscope Data')
legend('z (ground truth)', 'z (corrected)')