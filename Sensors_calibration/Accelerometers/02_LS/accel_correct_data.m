function corrected_data = accel_correct_data(raw_data,W,V,L)

[r,~] = size(raw_data);
[rw,cw] = size(W);
[rv,cv] = size(V);
[rl,cl] = size(L);

if r~=3
    error("raw_data must be a 3xN column vector")
end

if rw~=3||cw~=3
    error("W must be a 3x3 matrix")
end

if rv~=3||cv~=1
    error("V must be a 3x1 column vector")
end

if rl~=3||cl~=1
    error("L must be a 3x1 column vector, use L = [0;0;0] if you don't want cubic non linearity, use L=[0;0;0]")
end
    corrected_data = W*raw_data + V + L'*raw_data;
end