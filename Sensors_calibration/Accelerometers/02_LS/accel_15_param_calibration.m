function [W, V, L, E] = accel_15_param_calibration(g,N,cardanAngles,typeAngle)

[r,c] = size(g);
[nangle] = length(cardanAngles);

x = g(:,1);
y = g(:,2);
z = g(:,3);

if r ~= N 
    error("The number of orientations and row of G must coincide, Insert a valid set of orientation, g must be N x 3 Matrix")
end

if c ~= 3
    error("Insert a valid set of orientation, g must be N x 3 Matrix")
end

if N < 4 || nangle < 4
    error("the orientations must be at least 4")
end 

if strcmp(typeAngle,'deg')
    cardanAngles = 2*pi*cardanAngles / 360;
elseif strcmp(typeAngle,'rad')
else 
    error("angle type must be 'deg' or 'rad'")
end

Xx = [ g, ones([N,1]),x.^3];
Xy = [ g, ones([N,1]),y.^3];
Xz = [ g, ones([N,1]),z.^3];

Yx = -sin(cardanAngles(:,1));
Yy = cos(cardanAngles(:,1)).*sin(cardanAngles(:,2));
Yz = cos(cardanAngles(:,1)).*cos(cardanAngles(:,2));

bx = Xx\Yx;
by = Xy\Yy;
bz = Xz\Yz;

V = [bx(4);by(4);bz(4)];
L = [bx(5);by(5);bz(5)];
W = [bx(1:3)';by(1:3)';bz(1:3)'];  

Rx = Yx - Xx*bx;
Ry = Yy - Xy*by;
Rz = Yz - Xz*bz;
E = [Rx'*Rx , Ry'*Ry ,Rz'*Rz];
