function [ W , V , L] = temp_compensation(W1,W2,V1,V2,L1,L2,T,T1,T2) 
alfa = (T2-T/T2-T1);
beta = (T1-T/T2-T1);
W = alfa*W1 + beta*W2 ;
V = alfa*V1 + beta*V2 ;
L = alfa*L1 + beta*L2 ;
end