clearvars
close all
clc

addpath('../01_data/')
addpath('data_BMX')
%% Load data
msgbox("Change fs: sampling frequency, and N: number of position in the file")
% Insert sampling frequency here
fs = 100;
Ts = 1/fs;

% Insert number of positions here
N = 6;

%filename = 'MPU9250-1.txt';
%raw_data = importdata(filename);

file_x_down = readtable('pos_1.csv');
file_z_down = readtable('pos_2.csv');
file_x_up = readtable('pos_3.csv');
file_y_down = readtable('pos_4.csv');
file_y_up = readtable('pos_5.csv');
file_z_up = readtable('pos_6.csv');

x_up = file_x_up{1:1578,10:12};
x_down = file_x_down{1:1578,10:12};
y_up = file_y_up{1:1578,10:12};
y_down = file_y_down{1:1578,10:12};
z_up = file_z_up{1:1578,10:12};
z_down = file_z_down{1:1578,10:12};

%Gf = [x_up,x_down,y_up, y_down,z_up, z_down];
m_x_up = mean(x_up)*9.81;
m_x_down = mean(x_down)*9.81;
m_y_up = mean(y_up)*9.81;
m_y_down = mean(y_down)*9.81;
m_z_up = mean(z_up)*9.81;
m_z_down = mean(z_down)*9.81;
%{
subplot(3,1,1);
hold on;
scatter(0:1:(length(x_up)-1),x_up(:,1));
grid on
xlabel('Time') 
ylabel('Y(m)')
hold off
%}
%{
%% Manual steady state detection

titles = ["X postiive", "X negative", "Y positive", "Y negative", "Z positive", "Z negative"];
for i = 7:N
    titles(i)="general orientation";
end
figure
plot(x_down)
hold on
%plot(x_down)
grid minor
%plot(y_up)
plot(y_down)
%plot(z_up)
plot(z_down)
yl = ylim;
legend("x", "y", "z")
xlabel("Time [Samples]")
ylabel("Acceleration [g]")

x_ranges = zeros(N,2);
cardanAngles = zeros(N,2);
%cardanAngles(1:6,:) = 
G = zeros(N,3);

prompt = {'enter roll angle','enter pitch angle'};

for i = 1:N
    title("Select " + titles(i) + " start and end");
    [x_temp,~] = ginput(2);
    x_ranges(i,1:2) = floor(x_temp);
    rectangle('position', [x_ranges(i,1), yl(1), x_ranges(i,2)-x_ranges(i,1), yl(2)-yl(1)], 'facecolor', [0 0 0 0.1])
   
    G(i,:) = mean(Gf(x_ranges(i,1):x_ranges(i,1),:),1);
end
close
%}
%% 6 param Cal
G = [m_x_up;
    m_x_down;
    m_y_up;
    m_y_down;
    m_z_up;
    m_z_down;];
    
[W_6,V_6] = accel_6_param_calibration(G,N);
display(W_6)
display(V_6)

x_up_corrected = accel_correct_data(x_up',W_6,V_6,[0;0;0])';
x_up_corr_g = x_up_corrected * 9.81;

%{
subplot(3,1,1);
hold on;
scatter(0:1:(length(x_up)-1),x_up(:,1),0:1:(length(x_up)-1),x_up_corrected(:,1));
grid on
xlabel('Time') 
ylabel('Y(m)')
%hold off
%}