function [W, V, E] = accel_12_param_calibration(g,N,cardanAngles,typeAngle)

[r,c] = size(g);
[nangle] = length(cardanAngles);

if r ~= N 
    error("The number of orientations and row of G must coincide, Insert a valid set of orientation, g must be N x 3 Matrix")
end

if c ~= 3
    error("Insert a valid set of orientation, g must be N x 3 Matrix")
end

if N < 4 || nangle < 4
    error("the orientations must be at least 4")
end 

if strcmp(typeAngle,'deg')
    cardanAngles = 2*pi*cardanAngles / 360;
elseif strcmp(typeAngle,'rad')
else 
    error("angle type must be 'deg' or 'rad'")
end

X = [ g, ones([N,1])];

Yx = -sin(cardanAngles(:,1));
Yy = cos(cardanAngles(:,1)).*sin(cardanAngles(:,2));
Yz = cos(cardanAngles(:,1)).*cos(cardanAngles(:,2));

bx = X\Yx;
by = X\Yy;
bz = X\Yz;

V = [bx(4);by(4);bz(4)];
W = [bx(1:3)';by(1:3)';bz(1:3)'];  

Rx = Yx - X*bx;
Ry = Yy - X*by;
Rz = Yz - X*bz;
E = [Rx'*Rx , Ry'*Ry ,Rz'*Rz];