clearvars
close all
clc

addpath('../01_data/')

%% Load data
msgbox("Change fs: sampling frequency, and N: number of position in the file")
% Insert sampling frequency here
fs = 100;
Ts = 1/fs;

% Insert number of positions here
N = 14;

filename = 'MPU9250-1.txt';
raw_data = importdata(filename);
x = raw_data(:,1);
y = raw_data(:,2);
z = raw_data(:,3);

Gf = [x, y, z];

%% Manual steady state detection

titles = ["X postiive", "X negative", "Y positive", "Y negative", "Z positive", "Z negative"];
for i = 7:N
    titles(i)="general orientation";
end
figure
plot(x)
hold on
grid minor
plot(y)
plot(z)
yl = ylim;
legend("x", "y", "z")
xlabel("Time [Samples]")
ylabel("Acceleration [g]")

x_ranges = zeros(N,2);
cardanAngles = zeros(N,2);
%cardanAngles(1:6,:) = 
G = zeros(N,3);

prompt = {'enter roll angle','enter pitch angle'};

for i = 1:N
    title("Select " + titles(i) + " start and end");
    [x_temp,~] = ginput(2);
    x_ranges(i,1:2) = floor(x_temp);
    rectangle('position', [x_ranges(i,1), yl(1), x_ranges(i,2)-x_ranges(i,1), yl(2)-yl(1)], 'facecolor', [0 0 0 0.1])
    if i>7
        cardanAngles(i,:) = str2double(inputdlg(prompt,'orientation'));
    end
    
    G(i,:) = mean(Gf(x_ranges(i,1):x_ranges(i,1),:),1);
end
close


%% 6 param Cal

[W_6,V_6] = accel_6_param_calibration(G,N);
display(W_6)
display(V_6)

%% 12 param Cal

[W_12,V_12,E_12] = accel_12_param_calibration(G,N,cardanAngles,'deg');
display(W_12)
display(V_12)

%% 12 param + cubic non linearity

[W_15,V_15,L_15,E_15] = accel_15_param_calibration(G,N,cardanAngles,'deg');
display(W_15)
display(V_15)
display(L_15)

%% Choose the calibration to be evaluated 
W_cal = W_12;
V_cal = V_12;
L_cal = [0;0;0];

%% Save output
save(['../01_data/calib_output/', datestr(date())])
param_file = '24-Mar-2020';
normErr = calibration_evaluation(filename,param_file);

if norm(E_15) < norm(E_12) 
    msgbox("the best calibration is the 12 parameters + cubic non lin one, with error E = " +norm(E_15)+" ","best method")
else 
    msgbox("the best calibration is the 12 parameters one, with error E = " +norm(E_12)+" ","best method")
end
