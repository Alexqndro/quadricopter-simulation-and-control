function [W, V, E] = accel_6_param_calibration_full(g,N,cardanAngles,typeAngle)

[r,c] = size(g);
[nangle] = length(cardanAngles);

if r ~= N 
    error("The number of orientations and row of G must coincide, Insert a valid set of orientation, g must be N x 3 Matrix")
end

if c ~= 3
    error("Insert a valid set of orientation, g must be N x 3 Matrix")
end

if N < 4 || nangle < 4
    error("the orientations must be at least 4")
end 

if strcmp(typeAngle,'deg')
    cardanAngles = 2*pi*cardanAngles / 360;
elseif strcmp(typeAngle,'rad')
else 
    error("angle type must be 'deg' or 'rad'")
end

Xx = [ g(:,1), ones([N,1])];
Xy = [ g(:,2), ones([N,1])];
Xz = [ g(:,3), ones([N,1])];

Yx = -sin(cardanAngles(:,1));
Yy = cos(cardanAngles(:,1)).*sin(cardanAngles(:,2));
Yz = cos(cardanAngles(:,1)).*cos(cardanAngles(:,2));

bx = Xx\Yx;
by = Xy\Yy;
bz = Xz\Yz;

V = [bx(2);by(2);bz(2)];
W = [bx(1);by(1);bz(1)];  

Rx = Yx - Xx*bx;
Ry = Yy - Xy*by;
Rz = Yz - Xz*bz;
E = [Rx'*Rx , Ry'*Ry ,Rz'*Rz];