function normErr = calibration_evaluation(filename, calibration_parameters)

addpath('../01_data/')
addpath('../01_data/calib_output')

load(calibration_parameters,'W_cal','V_cal','L_cal');

raw_data = importdata(filename);
x = raw_data(:,1);
y = raw_data(:,2);
z = raw_data(:,3);

Gf = [x, y, z];
normErr = 0;
corrected_data = zeros(size(Gf));
n = zeros(length(Gf));
n_corrected  = zeros(length(Gf));
display(W_cal)

for i = 1:length(Gf)
    corrected_data(i,:) = accel_correct_data(Gf(i,:)',W_cal,V_cal,L_cal);
    n(i) = norm(Gf(i,:));
    n_corrected(i) = norm(corrected_data(i,:));
    normErr = normErr + (1-n_corrected(1));
end

figure
subplot(3,1,1)
hold on
plot(Gf(:,1))
plot(corrected_data(:,1))
ylabel("X [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")
subplot(3,1,2)
hold on
plot(Gf(:,2))
plot(corrected_data(:,2))
ylabel("Y [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")
subplot(3,1,3)
hold on
plot(Gf(:,3))
plot(corrected_data(:,3))
ylabel("Z [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")

figure
plot(n)
hold on
plot(n_corrected)
ylabel("Norm [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")

figure
plot(movmean(n, 100))
hold on
plot(movmean(n_corrected,100))
ylabel("Norm [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")