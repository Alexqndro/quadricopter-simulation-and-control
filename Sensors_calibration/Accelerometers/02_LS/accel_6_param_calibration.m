function [W, V] = accel_6_param_calibration(g,N)

[r,c] = size(g);

if r ~= N 
    error("The number of orientations and row of G must coincide, Insert a valid set of orientation, g must be N x 3 Matrix")
end

if c ~= 3
    error("Insert a valid set of orientation, g must be N x 3 Matrix")
end

if N < 4
    error("the orientations must be at least 6, the six orientation are Xup Xdown, Yup, Ydown, Zup, Zdown")
end 

W_xx = 2*9.81/(g(1,1) - g(2,1));
Vx = -(g(1,1)+g(2,1))/(g(1,1)-g(2,1));

W_yy = 2*9.81/(g(3,2) - g(4,2));
Vy = -(g(3,2)+g(4,2))/(g(3,2)-g(4,2));

W_zz = 2*9.81/(g(5,3) - g(6,3));
Vz = -(g(5,3)+g(6,3))/(g(5,3)-g(6,3));

W = [W_xx, 0, 0; 0, W_yy, 0; 0, 0, W_zz];
V = [Vx; Vy; Vz];

end
