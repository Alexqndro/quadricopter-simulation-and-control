%% Setup
clear
close all
clc

addpath('../01_data/')
addpath('../01_data/calib_output')

%% Load data

% Insert gravity value at location here
g = 9.81;

% Calibration data
load('20-Mar-2020');

% Evalution dataset
filename = 'LSM9DS1-static_orientations.txt';
raw_data = importdata(filename);
x = raw_data(:,2)/g;
y = raw_data(:,3)/g;
z = raw_data(:,4)/g;

Gf = [x, y, z];


%% Output evaluation

for i = 1:length(Gf)
    corrected_data(i,:) = accel_correct_data(Gf(i,:)', accel_param);
    n(i) = norm(Gf(i,:));
    n_corrected(i) = norm(corrected_data(i,:));
end

figure
subplot(3,1,1)
hold on
plot(Gf(:,1))
plot(corrected_data(:,1))
ylabel("X [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")
subplot(3,1,2)
hold on
plot(Gf(:,2))
plot(corrected_data(:,2))
ylabel("Y [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")
subplot(3,1,3)
hold on
plot(Gf(:,3))
plot(corrected_data(:,3))
ylabel("Z [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")

figure
plot(n)
hold on
plot(n_corrected)
ylabel("Norm [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")

figure
plot(movmean(n, 100))
hold on
plot(movmean(n_corrected,100))
ylabel("Norm [g]")
xlabel("Time [samples]")
legend("Raw", "Corrected")