function fun = accel_cost_func(orientation_data, param)
% ACCEL_COST_FUN Cost function for accelerometer calibration
% orientation_data: 3xN_orientations matrix with gravity vectory measurements
% param:            Parameters for measurements correction
[r, c] = size(orientation_data);
p = length(param);
    if r~=3
        error("orientation_data must be a 3xn matrix") 
    elseif p~=9
        error("param must be a vector of length 9") 
    else
        fun = 0;
        for k=1:c
            fun = fun + (1 - norm(accel_correct_data((orientation_data(:,k)),param))^2);
        end
    end
end