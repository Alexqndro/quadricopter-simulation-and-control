%% Setup
clear
close all
clc

addpath('../01_data')

%% Load data

% Insert gravity value at location here
g = 9.81;

% Insert sampling frequency here
fs = 100;
Ts = 1/fs;

% Insert number of positions here
N = 14;

filename = 'MPU9250-1.txt';
raw_data = importdata(filename);
x = raw_data(:,1);
y = raw_data(:,2);
z = raw_data(:,3);

Gf = [x, y, z];

%% Automatic steady state detection - WIP
%
% f = 10;             % [Hz] Frequency of derivator cutoff
% threshold = 0.05;   % Threshold for steady state detection
%
% s = tf('s');
% filter_ct = s/(s/(2*pi*f)+1)^2;
% filter_dt = c2d(filter_ct, Ts);
% [filter_num, filter_den] = tfdata(filter_dt);
% filter_num = filter_num{1};
% filter_den = filter_den{1};
%
% x_d = filter(filter_num, filter_den, x);
% y_d = filter(filter_num, filter_den, y);
% z_d = filter(filter_num, filter_den, z);
%
% steady_state = [];
% for i = 1:length(x_d)
%     if abs(x_d(i)) < 0.05 && abs(y_d(i)) < 0.05 && abs(z_d(i)) < 0.05
%         if abs(x(i))>0.5 && abs(y(i))<0.5 && abs(z(i))<0.5
%             steady_state = [steady_state, i];
%         end
%     end
% end
%
% plot(t(steady_state(2:end)), diff(steady_state))
% ylim([0,2])
% no_steady_state = setdiff(1:length(x),steady_state);
% figure
% plot(t(no_steady_state), x_d(no_steady_state),'x')
% hold on
% plot(t(steady_state), x_d(steady_state), 'x')
%
% figure
% plot(t(no_steady_state), x(no_steady_state), 'x')
% hold on
% plot(t(steady_state), x(steady_state), 'x')


%% Manual steady state detection

titles = ["X postiive", "X negative", "Y positive", "Y negative", "Z positive", "Z negative"];
figure
plot(x)
hold on
grid minor
plot(y)
plot(z)
yl = ylim;
legend("x", "y", "z")
xlabel("Time [Samples]")
ylabel("Acceleration [g]")
for i = 1:6
    title(["Select " + titles(i) + " start and end"]);
    [x_temp,~] = ginput(2);
    x_ranges(i,1:2) = floor(x_temp);
    rectangle('position', [x_ranges(i,1), yl(1), x_ranges(i,2)-x_ranges(i,1), yl(2)-yl(1)], 'facecolor', [0 0 0 0.1])
end
close

x_pos_start = x_ranges(1,1);
x_pos_end   = x_ranges(1,2);
x_neg_start = x_ranges(2,1);
x_neg_end   = x_ranges(2,2);
y_pos_start = x_ranges(3,1);
y_pos_end   = x_ranges(3,2);
y_neg_start = x_ranges(4,1);
y_neg_end   = x_ranges(4,2);
z_pos_start = x_ranges(5,1);
z_pos_end   = x_ranges(5,2);
z_neg_start = x_ranges(6,1);
z_neg_end   = x_ranges(6,2);

%% Compute bias and gain

Gxp = mean(Gf(x_pos_start:x_pos_end,:))';
Gxn = mean(Gf(x_neg_start:x_neg_end,:))';
Gyp = mean(Gf(y_pos_start:y_pos_end,:))';
Gyn = mean(Gf(y_neg_start:y_neg_end,:))';
Gzp = mean(Gf(z_pos_start:z_pos_end,:))';
Gzn = mean(Gf(z_neg_start:z_neg_end,:))';

W_xx = 2/(Gxp(1) - Gxn(1));
Vx = -(Gxp(1)+Gxn(1))/(Gxp(1)-Gxn(1));

W_yy = 2/(Gyp(2) - Gyn(2));
Vy = -(Gyp(2)+Gyn(2))/(Gyp(2)-Gyn(2));

W_zz = 2/(Gzp(3) - Gzn(3));
Vz = -(Gzp(3)+Gzn(3))/(Gzp(3)-Gzn(3));

W = [W_xx, 0, 0; 0, W_yy, 0; 0, 0, W_zz];
V = [Vx; Vy; Vz];

disp("### Step 1 ###")
disp("Gain matrix: ")
disp(W)
disp("Offsets vector: ")
disp(V)

%% Steady state selection
% Select all the steady state parts

% 3xN_orientations matrix with averaged steady state data
orientations_data(:,1) = Gxp;
orientations_data(:,2) = Gxn;
orientations_data(:,3) = Gyp;
orientations_data(:,4) = Gyn;
orientations_data(:,5) = Gzp;
orientations_data(:,6) = Gzn;

figure
plot(x)
hold on
grid minor
plot(y)
plot(z)
yl = ylim;
legend("x", "y", "z")
xlabel("Time [Samples]")
ylabel("Acceleration [g]")
for i = 1:N
    if i > 6
        title(["Select " + i + " start and end"]);
        [x_temp,~] = ginput(2);
        x_ranges(i,1:2) = floor(x_temp);
    end
    rectangle('position', [x_ranges(i,1), yl(1), x_ranges(i,2)-x_ranges(i,1), yl(2)-yl(1)], 'facecolor', [0 0 0 0.1])
end
close

%% Optimize
% Initial guess: all axis ortogonal and gain/baiases from previous step
param0 = [0 0 0 W_xx, W_yy, W_zz, Vx, Vy, Vz];


f =  @(param)accel_cost_func(orientations_data, param);
options = optimoptions(@fminunc,'Display','iter');
accel_param = fminunc(@(param)f(param), param0, options);


T = [ 1    -accel_param(1)    accel_param(2);
      0          1           -accel_param(3);
      0          0                 1          ];

S = [ accel_param(4)      0              0;
           0         accel_param(5)      0;
           0              0         accel_param(6)  ];

b = [ accel_param(7);
      accel_param(8);
      accel_param(9)  ];

disp("### Step 2 ###")
disp("Gain matrix: ")
disp(S)
disp("Offsets vector: ")
disp(b)
disp("Non orthogonality matrix: ")
disp(T)

%% Save output
save(['../01_data/calib_output/', datestr(date())])
