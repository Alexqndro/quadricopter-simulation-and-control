function corrected_data = accel_correct_data(raw_data, param)
% ACCEL_CORRECT_DATA Corrects the accelerometer data using a 9 parameters
% model
[r,c] = size(raw_data);
p = length(param);
if or(r~=3,c~=1)
    error("raw_data must be a 3x1 column vector")
elseif p~=9
    error("param must be a vector of length 9")
else
    Error_Axes_YZ = param(1);
    Error_Axes_ZY = param(2);
    Error_Axes_XZ = param(3);
    Scale_X       = param(4);
    Scale_Y       = param(5);
    Scale_Z       = param(6);
    Bias_X        = param(7);
    Bias_Y        = param(8);
    Bias_Z       = param(9);
    
    T = [ 1    -Error_Axes_YZ    Error_Axes_ZY;
          0          1          -Error_Axes_XZ;
          0          0                1          ];
    
    S = [ Scale_X     0        0;
             0     Scale_Y     0;
             0        0      Scale_Z  ];
    
    b = [ Bias_X;
          Bias_Y;
          Bias_Z  ];
    
    corrected_data = T*S*(raw_data+b);
end
end